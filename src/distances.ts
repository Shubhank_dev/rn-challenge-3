import * as Location from 'expo-location';
import { Branch } from './Branch';

type Coordinate = {
  latitude: number;
  longitude: number;
};

// https://stackoverflow.com/a/65799152/5225716
function distanceBetween(coord1: Coordinate, coord2: Coordinate) {
  if (
    coord1.latitude == coord2.latitude &&
    coord1.longitude == coord2.longitude
  ) {
    return 0;
  }

  const radlat1 = (Math.PI * coord1.latitude) / 180;
  const radlat2 = (Math.PI * coord2.latitude) / 180;
  const theta = coord1.longitude - coord2.longitude;
  const radtheta = (Math.PI * theta) / 180;

  let dist =
    Math.sin(radlat1) * Math.sin(radlat2) +
    Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

  if (dist > 1) {
    dist = 1;
  }

  dist = Math.acos(dist);
  dist = (dist * 180) / Math.PI;
  dist = dist * 60 * 1.1515;
  dist = dist * 1.609344; //convert miles to km

  return dist;
}

export function closest5BranchTo(
  location: Location.LocationGeocodedLocation,
  branches: Branch[],
): Branch[] | undefined {

  branches.sort((branch1: Branch, branch2: Branch) => {
    if (branch1.PostalAddress.GeoLocation && branch2.PostalAddress.GeoLocation) {
      const distanceFromBranch1 = distanceBetween(location, {
        latitude: parseFloat(
          branch1.PostalAddress.GeoLocation.GeographicCoordinates.Latitude,
        ),
        longitude: parseFloat(
          branch1.PostalAddress.GeoLocation.GeographicCoordinates.Longitude,
        ),
      });

      const distanceFromBranch2 = distanceBetween(location, {
        latitude: parseFloat(
          branch2.PostalAddress.GeoLocation.GeographicCoordinates.Latitude,
        ),
        longitude: parseFloat(
          branch2.PostalAddress.GeoLocation.GeographicCoordinates.Longitude,
        ),
      });
      
      if (distanceFromBranch1 < distanceFromBranch2) {
        return -1
      } else if (distanceFromBranch2 < distanceFromBranch1) {
        return 1
      }
    }

    return 0
  })
  
  // need only first 5 closest branch
  return branches.slice(0, 5);
}
