import MapView, { Marker, Callout, LatLng } from 'react-native-maps';
import { StyleSheet, View, Text, StyleProp, ViewStyle } from 'react-native';
import { useEffect, useRef } from 'react';
import { Branch, branchAddress } from './Branch';

export default function Map({ closest, mapStyle }: { closest: Branch[] | undefined, mapStyle: StyleProp<ViewStyle> }) {

  const mapRef = useRef(null)

  // animate to the region of markers when we get a change of results
  useEffect(() => {
    const markersKeyArr = closest?.map((closestBranch) => closestBranch.Identification)

    if (mapRef)
      mapRef.current.fitToSuppliedMarkers(markersKeyArr)
  }, [closest])

  const renderMarkers = () => {
    if (closest) {
      return closest.map((branch: Branch) => {
        if (branch.PostalAddress.GeoLocation) {
          return (
            <Marker
            identifier={branch.Identification}
            key={branch.Identification}
            title={branch.Name}
            description={branchAddress(branch)}
            coordinate={{
              latitude: parseFloat(
                branch.PostalAddress.GeoLocation.GeographicCoordinates
                  .Latitude,
              ),
              longitude: parseFloat(
                branch.PostalAddress.GeoLocation.GeographicCoordinates
                  .Longitude,
              ),
            }}>
            <Callout tooltip>
              <View style={styles.callout}>
                <Text style={styles.calloutHeader}>
                  {branch.Name || branch.Identification}
                </Text>
                <Text style={styles.calloutText}>{branchAddress(branch)}</Text>
              </View>
            </Callout>
          </Marker>
          )
        }         
      })
    }
  }
  
  return (
    <View style={styles.container}>
      <MapView
        ref={mapRef}
        style={[styles.map, mapStyle]}
        initialRegion={{
          latitude: 55.77,
          latitudeDelta: 11.03,
          longitude: -2.82,
          longitudeDelta: 11.35,
        }}>
        {renderMarkers()}
      </MapView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: StyleSheet.absoluteFillObject,
  callout: {
    padding: 5,
    backgroundColor: '#ffffffa0',
    borderRadius: 4,
  },
  calloutHeader: {
    fontFamily: 'textBold',
    color: 'black',
    fontSize: 14,
  },
  calloutText: {
    fontFamily: 'textRegular',
    color: 'black',
    fontSize: 10,
  },
});
