import { FlatList, StyleSheet, View } from 'react-native';
import { Branch } from './Branch';
import BranchDetails from './BranchDetails';


export default function SearchResults({ closestBranches }: { closestBranches: Branch[] | undefined }) {

    const renderResultItem = (({ item }: { item: Branch }) => <BranchDetails branch={item}/>);

    return (
        <FlatList
            data={closestBranches}
            contentContainerStyle={styles.flatListContainerStyle}
            renderItem={renderResultItem}
            keyExtractor={(item: Branch) => item.Identification }
        />
    )
}

const styles = StyleSheet.create({
    // to avoid iPhone bottom safe area
   flatListContainerStyle: {
    paddingBottom: 64,
   }
})