import { StyleSheet, View, Text } from 'react-native';
import { Branch } from './Branch';

export default function BranchDetails({ branch }: { branch: Branch }) {

  // could be improved
  const getAddress = (branch: Branch): String => {
    const { BuildingNumber, StreetName, TownName, PostCode } = branch.PostalAddress
    return `${BuildingNumber}, ${StreetName}, ${TownName}, ${PostCode}`
  }

  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <Text style={styles.textRedBold}>{branch.Name}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.text}>{getAddress(branch)}</Text>
      </View>
      {branch.ServiceAndFacility && (
        <View style={styles.row}>
          <Text style={styles.text}>
            Services:<Text style={styles.textBold}>{branch.ServiceAndFacility.join(', ')}</Text>
          </Text>
        </View>
      )}
      {branch.Accessibility && (
        <View style={styles.row}>
          <Text style={styles.text}>
            Accessibility:<Text style={styles.textBold}>{branch.Accessibility.join(', ')}</Text>
          </Text>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    marginHorizontal: 20,
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    fontFamily: 'textRegular',
    color: 'black',
    fontSize: 14,
    flex: 1,
  },
  textBold: {
    fontFamily: 'textBold',
    color: 'black',
    fontSize: 14,
    flex: 1,
  },
  textRedBold: {
    fontFamily: 'textBold',
    color: 'red',
    fontSize: 16,
    flex: 1,
  },
});
