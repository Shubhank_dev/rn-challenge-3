import { StyleSheet, Text, View } from 'react-native';
import Spinner from './Spinner';
import { SearchLocation } from './SearchLocation';

export default function InputResults({
  search,
  input,
}: {
  search: SearchLocation;
  input: string;
}) {
  if (!input.trim() || search === undefined) return null;
  if (search === 'fetching') {
    return (
      <>
        <Spinner height={20} />
        <Text style={styles.text}>
          &nbsp;&nbsp;Searching for{' '}
          <Text style={styles.inputText}>{input}</Text>
        </Text>
      </>
    );
  }
  if (search === 'no-result') {
    return (
      <Text style={styles.text}>
        No results for <Text style={styles.inputText}>{input}</Text>
      </Text>
    );
  }
  if (search === 'error') {
    return (
      <Text style={styles.text}>
        Error retrieving results for{' '}
        <Text style={styles.inputText}>{input}</Text>
      </Text>
    );
  }
  return (
    <View style={styles.searchTextContainer}>
      <Text style={styles.text}>
        Showing results for <Text style={styles.inputText}>{input}</Text>
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  searchTextContainer: {
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
    paddingBottom: 10,
    flex: 1,
  },
  text: {
    fontFamily: 'textRegular',
    fontSize: 16,
    color: '#000',
  },
  inputText: {
    fontFamily: 'textBold',
  },
});
