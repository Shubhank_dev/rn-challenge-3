import { useEffect, useState } from 'react';
import { Animated, Dimensions, StyleSheet, Text, View } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import BranchesInput from './src/BranchesInput';
import { SearchLocation } from './src/SearchLocation';
import Map from './src/Map';
import Spinner from './src/Spinner';
import useLoading from './src/useLoading';
import { Branch } from './src/Branch';
import { closest5BranchTo } from './src/distances';
import SearchResults from './src/SearchResults';
import { useKeyboardHeight } from './src/useKeyboardHeight';

export default function App() {
  const [state, branches] = useLoading();
  const [search, setSearch] = useState<SearchLocation>();
  const [closest, setClosest] = useState<undefined | Branch[]>();
  const keyboardHeight = useKeyboardHeight() // animate input field when keyboard comes up

  useEffect(() => {
    if (branches && typeof search === 'object') {      
      const closestBranches = closest5BranchTo(search, branches)
      setClosest(closestBranches);
    } else {
      setClosest(undefined);
    }
  }, [search, branches]);
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Map closest={closest} mapStyle={styles.mapStyle}/>
      {state === 'ready' ? (
        <>
          <Animated.View style={[styles.resultsContainer,{ bottom: keyboardHeight }]}>
            <BranchesInput search={search} setSearch={setSearch} />
            <SearchResults closestBranches={closest} />
          </Animated.View>
        </>
      ) : state === 'error' ? (
        <View style={styles.centred}>
          <Text style={styles.error}>An error has occurred</Text>
        </View>
      ) : (
        <View style={styles.centred}>
          <Spinner height={60} />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  mapStyle: {
    height: Dimensions.get('screen').height * 0.6,
  },
  resultsContainer: {
    backgroundColor: 'white',
    marginTop: 'auto',
    height: Dimensions.get('screen').height * 0.4,
  },
  error: {
    fontFamily: 'textRegular',
    fontSize: 24,
    color: '#ED0000',
    padding: 10,
    backgroundColor: '#80808030',
  },
  centred: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
